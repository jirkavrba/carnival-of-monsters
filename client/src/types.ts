
export enum LandType {
    Aerie = "aerie",
    Caves = "caves",
    Depths = "depths",
    Forest = "forest",
    Darklands = "darklands",
    Dreamlands = "dreamlands"
}

export interface Land {
    type: LandType,
    points: number,
    requiredPoints: number,
    morphedFrom?: Land
}