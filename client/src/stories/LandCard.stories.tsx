import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import LandCard, { LandCardProps } from "../components/LandCard";
import { LandType } from "../types";

export default {
    title: "LandCard",
    component: LandCard
} as ComponentMeta<typeof LandCard>

const Template: ComponentStory<typeof LandCard> = (props: LandCardProps) => <LandCard {...props}/>

export const BasicLandCard = Template.bind({})
export const DistantLandCard = Template.bind({})
export const MorphedLandCard = Template.bind({})

BasicLandCard.args = {
    land: {
        type: LandType.Aerie,
        points: 1,
        requiredPoints: 0,
    }
}

DistantLandCard.args = {
    land: {
        type: LandType.Depths,
        points: 3,
        requiredPoints: 2,
    }
}

MorphedLandCard.args = {
    land: {
        type: LandType.Dreamlands,
        points: 1,
        requiredPoints: 0,
        morphedFrom: {
            type: LandType.Forest,
            points: 2,
            requiredPoints: 1
        }
    }
}
