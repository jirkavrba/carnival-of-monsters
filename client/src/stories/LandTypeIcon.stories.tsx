import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import LandTypeIcon, { LandTypeIconProps } from "../components/LandTypeIcon";
import { LandType } from "../types";

export default {
    title: "LandTypeIcon",
    component: LandTypeIcon
} as ComponentMeta<typeof LandTypeIcon>

const Template: ComponentStory<typeof LandTypeIcon> = (props: LandTypeIconProps) => <LandTypeIcon {...props}/>

export const LandTypeIconStory = Template.bind({})
LandTypeIconStory.args = {
    type: LandType.Aerie,
    small: false
}