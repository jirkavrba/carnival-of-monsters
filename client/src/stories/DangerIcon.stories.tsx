import { ComponentMeta, ComponentStory } from "@storybook/react";
import DangerIcon, { DangerIconProps } from "../components/DangerIcon";
export default {
    title: "DangerIcon",
    component: DangerIcon
} as ComponentMeta<typeof DangerIcon>

const  Template: ComponentStory<typeof DangerIcon> = (props: DangerIconProps) => <DangerIcon {...props}/>

export const Danger = Template.bind({})
Danger.args = {
    count: 1,
    small: false
}
