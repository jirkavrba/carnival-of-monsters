import { ComponentMeta, ComponentStory } from "@storybook/react";
import ExtraCardIcon, { ExtraCardProps } from "../components/ExtraCardIcon";

export default {
    title: "ExtraCardIcon",
    component: ExtraCardIcon
} as ComponentMeta<typeof ExtraCardIcon>

const  Template: ComponentStory<typeof ExtraCardIcon> = (props: ExtraCardProps) => <ExtraCardIcon {...props}/>

export const ExtraCard = Template.bind({})
ExtraCard.args = {
    count: 1
}
