import { FC } from "react";
import { Land, LandType } from "../types";
import LandTypeIcon from "./LandTypeIcon";
import classNames from "classnames";

export interface LandCardProps {
    land: Land
}

const colors: Record<LandType, string> = {
    [LandType.Aerie]: "bg-amber-50",
    [LandType.Caves]: "bg-stone-50",
    [LandType.Darklands]: "bg-slate-50",
    [LandType.Depths]: "bg-sky-50",
    [LandType.Forest]: "bg-emerald-50",
    [LandType.Dreamlands]: "bg-violet-50"
};

const borders: Record<LandType, string> = {
    [LandType.Aerie]: "border-amber-400",
    [LandType.Caves]: "border-stone-500",
    [LandType.Darklands]: "border-slate-500",
    [LandType.Depths]: "border-sky-500",
    [LandType.Forest]: "border-emerald-500",
    [LandType.Dreamlands]: "border-violet-500"
}

const rings: Record<LandType, string> = {
    [LandType.Aerie]: "ring-amber-200",
    [LandType.Caves]: "ring-stone-200",
    [LandType.Darklands]: "ring-slate-200",
    [LandType.Depths]: "ring-sky-200",
    [LandType.Forest]: "ring-emerald-200",
    [LandType.Dreamlands]: "ring-violet-200"
}

interface LandTypeIconsProps {
    type: LandType,
    small: boolean,
    count: number,
    background: string
}

const LandTypeIcons: FC<LandTypeIconsProps> = ({type, small, count, background}: LandTypeIconsProps) => {
    if (count < 1) {
        return (<></>);
    }

    const margins = ["ml-0", "ml-3", "ml-6", "ml-9"];
    const indexes = ["z-40", "z-30", "z-20", "z-10"]
    const scales = ["scale-100", "scale-[.90]", "scale-[.80]", "scale-[.70]"];

    return (
        <div className="h-full relative">
            {[...new Array(count)].map((_, i) =>
                <div className={`absolute transform ${classNames(margins[i], indexes[i], scales[i], background)} p-1 my-3 rounded-full`} key={i}>
                    <LandTypeIcon type={type} small={small}/>
                </div>
            )}
        </div>
    );
};

export const LandCard: FC<LandCardProps> = ({land}: LandCardProps) => {
    const morphed = land.morphedFrom !== undefined
    const container = classNames(
        "w-32 h-24 rounded-2xl shadow-lg border-2 flex flex-row justify-between items-start p-2",
        colors[land.type],
        borders[land.type],
        {
            "ring-4": morphed,
            [rings[land.morphedFrom?.type || land.type]]: morphed
        }
    );

    return (
        <div className={container}>
            <LandTypeIcons type={land.type} small={false} count={land.points} background={colors[land.type]}/>
            <div className="h-full flex flex-col justify-center items-center" title={`Requires ${land.requiredPoints} land points`}>
                {[...new Array(land.requiredPoints)].map((_, i) =>
                    <LandTypeIcon type={land.type} small={true} key={i} className={"my-1"}/>
                )}
            </div>
        </div>
    );
}

export default LandCard;