import { FC } from "react";

export interface ExtraCardProps {
    count: number
}

export const ExtraCardIcon: FC<ExtraCardProps> = ({count}: ExtraCardProps) => {
    return (
        <div className="flex border-solid border-black items-center justify-center w-8 h-12 bg-gradient-to-b from-black to-gray-500 rounded-md text-white">
            {count}
        </div>
    );
};


export default ExtraCardIcon;