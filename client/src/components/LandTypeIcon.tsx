import { FC } from "react";
import { LandType } from "../types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp, library } from "@fortawesome/fontawesome-svg-core";
import { faFeatherPointed, faMountain, faSkull, faDroplet, faTree, faMoon } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";

// TODO: Resolve dynamic imports using babel import macro
library.add(faFeatherPointed, faMountain, faSkull, faDroplet, faTree, faMoon);

export interface LandTypeIconProps {
    type: LandType,
    small?: boolean,
    className?: string
}

const colors: Record<LandType, string> = {
    [LandType.Aerie]: "bg-amber-400 text-amber-100",
    [LandType.Caves]: "bg-stone-600 text-stone-200",
    [LandType.Darklands]: "bg-slate-600 text-slate-300",
    [LandType.Depths]: "bg-sky-600 text-sky-200",
    [LandType.Forest]: "bg-emerald-600 text-emerald-200",
    [LandType.Dreamlands]: "bg-violet-600 text-violet-200"
};

const icons: Record<LandType, IconProp> = {
    [LandType.Aerie]: "feather-pointed",
    [LandType.Caves]: "mountain",
    [LandType.Darklands]: "skull",
    [LandType.Depths]: "droplet",
    [LandType.Forest]: "tree",
    [LandType.Dreamlands]: "moon"
};

export const LandTypeIcon: FC<LandTypeIconProps> = ({type, small = false, className = ""}: LandTypeIconProps) => {
    const container = classNames(colors[type], {"opacity-60 w-4 h-4 shadow": small, "w-10 h-10": !small});
    const icon = classNames({"transform scale-50": small});

    return (
        <div className={`flex items-center justify-center rounded-full ${container} ${className}`}>
            <FontAwesomeIcon icon={icons[type]} className={icon}/>
        </div>
    );
}

export default LandTypeIcon;