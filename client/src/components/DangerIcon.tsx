import { library } from "@fortawesome/fontawesome-svg-core";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames";
import { FC } from "react";

export interface DangerIconProps {
    count: number,
    small: boolean
}
library.add(faTriangleExclamation);


export const DangerIcon: FC<DangerIconProps> = ({count, small}: DangerIconProps) => {
    const container = classNames({"opacity-60 w-4 h-4 shadow": small, "w-8 h-8": !small});
    const icon = classNames({"transform scale-50": small});

    
    return (
        <div className={`flex items-center justify-center rounded-full text-red-600 bg-red-300 text-lg ${container}`}>
               <FontAwesomeIcon icon="triangle-exclamation" className={icon}/>
        </div>
    );
};


export default DangerIcon;