package dev.vrba.carnival.repository

import dev.vrba.carnival.entity.Player
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PlayerRepository : CrudRepository<Player, Int> {

    fun findPlayerByToken(token: String): Player?

}