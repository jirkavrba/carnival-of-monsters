package dev.vrba.carnival

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CarnivalOfMonstersApplication

fun main(args: Array<String>) {
    runApplication<CarnivalOfMonstersApplication>(*args)
}
