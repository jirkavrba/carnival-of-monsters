package dev.vrba.carnival.entity

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table


@Table("players")
data class Player(
    @Id
    val id: Int = 0,

    @Column("username")
    val username: String,

    @Column("authentication_token")
    val token: String
)