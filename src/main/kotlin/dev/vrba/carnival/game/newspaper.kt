package dev.vrba.carnival.game


sealed class NewspaperAction(val reward: (Player) -> Player)

// Get one cage when applied
object CageAction : NewspaperAction({ it.copy(cages = it.cages + 1) })

// Get 1 gold for every land of a given type
class LandsRewardAction(val type: LandType) : NewspaperAction({
    it.copy(gold = it.gold + it.lands.count { land -> land.type == type })
})

// Get 2 gold for every character you employ
object CharactersRewardAction : NewspaperAction({
    it.copy(gold = it.gold + it.menagerie.count { card -> card is CharacterCard })
})

// Get 2 gold
object GoldAction : NewspaperAction({ it.copy(gold = it.gold + 2) })

// TODO: Implement revealing monsters from the kept pile