package dev.vrba.carnival.game

sealed class SecretLetterTask(val evaluation: (player: Player) -> Int)

// Get 4 victory points (no clickbait)
object FreeFourPointsTask : SecretLetterTask({ 4 })

// Get 2 victory points for every captured dreamlands monster
object DreamlandsTask : SecretLetterTask({
    it.menagerie
        .filterIsInstance<MonsterCard>()
        .count { card -> card.monster.type == LandType.Dreamlands } * 2
})

// Get 2 victory points for every danger symbol among monsters you have captured
object DangerSymbolsTask : SecretLetterTask({
    it.menagerie
        .filterIsInstance<MonsterCard>()
        .sumOf { card -> card.monster.danger } * 2
})

// Get 1 victory point for every captured monster with level 1
object Level1MonstersTask : SecretLetterTask({
    it.menagerie
        .filterIsInstance<MonsterCard>()
        .count { card -> card.monster.level == 1 }
})

// Get 2 victory points for every distant land (level > 1)
object DistantLandsTask : SecretLetterTask({
    it.lands.count { land -> land.type != LandType.Dreamlands && land.points > 1 }
})

// Get 2 victory points for every loan
object LoansRewardTask : SecretLetterTask({ it.loans * 2 })

// Get 2 victory points for every cage
object CagesRewardTask : SecretLetterTask({ it.cages * 2 })

// Get 2 victory points for every other secret letter
object LettersRewardTask : SecretLetterTask({
    (it.menagerie
        .filterIsInstance<SecretLetterTask>()
        .count() - 1) * 2
})

// Get 3 victory points for every character you employ
object CharactersRewardTask : SecretLetterTask({ it.characters.size * 3 })

// Get 6 victory points if you haven't taken any loans
object NoLoansRewardTask : SecretLetterTask({ if (it.loans == 0) 6 else 0 })

// Get 7 victory points if you have at least one land for every land type
object AllLandsExploredTask : SecretLetterTask({
    val available = LandType.values().size
    val explored = it.lands.groupBy { land -> land.type }.size

    if (explored == available) 7 else 0
})

// Get 7 victory points if you have explored < 3 land types
// Get 4 victory points if you have explored 4 land types
object FewLandsExploredTask : SecretLetterTask({
    when (it.lands.groupBy { land -> land.type }.size) {
        1, 2, 3 -> 7
        4 -> 4
        else -> 0
    }
})

// Get 2 victory points for every level 3 monster
// Get 3 victory points for every level 4+ monster
object StrongMonstersTask : SecretLetterTask({
    it.menagerie
        .filterIsInstance<MonsterCard>()
        .sumOf { card ->
            @Suppress("USELESS_CAST")
            when (card.monster.level) {
                1, 2 -> 0
                3 -> 2
                else -> 3
            } as Int
        }
})

// Get 7 victory points if you have one land type explored at least to level 7
object DeeplyExploredLandTask : SecretLetterTask({
    val levels = it.lands
        .groupBy { land -> land.type }
        .map { (_, values) -> values.sumOf { card -> card.points } }

    if (levels.any { level -> level >= 7 }) 7 else 0
})

// Get 2 victory points for every captured monster with the specified type
class MonstersWithLandTypeTask(val type: LandType) : SecretLetterTask({
    it.menagerie
        .filterIsInstance<MonsterCard>()
        .count { card -> card.monster.type === type } * 2
})