package dev.vrba.carnival.game

sealed interface GameCard

open class ScoredGameCard(val points: Int) : GameCard

class LandCard(val land: Land) : GameCard

class MonsterCard(val monster: Monster) : ScoredGameCard(monster.points)

class CharacterCard(val character: Character) : GameCard

class SecretLetterCard(val task: SecretLetterTask) : GameCard

class SeasonalRewardCard(val type: LandType) : ScoredGameCard(3)

class NewspaperCard(val action: NewspaperAction) : GameCard

object CageCard : ScoredGameCard(1)