package dev.vrba.carnival.game

data class Character(
    val cost: Int,
    val effect: CharacterEffect
)

sealed interface CharacterEffect

// Character is turned into a land with a chosen land type
object LandPlaceholder : CharacterEffect

// Every season, player can ignore one danger symbol among captured creatures
object IgnoreDangerSymbol : CharacterEffect

// Player is rewarded for casting creatures with danger symbols
object DangerSymbolsReward : CharacterEffect

// Player is rewarded for playing newspaper cards
object NewspaperRewards : CharacterEffect

// Player is rewarded for casting creatures with the specified land type
class LandTypeRewards(val type: LandType) : CharacterEffect
