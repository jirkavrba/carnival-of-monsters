package dev.vrba.carnival.game

data class Land(
    val type: LandType,
    val points: Int,
    val requiredPoints: Int,
    // If this land was morphed into a dreamland
    val morphedFrom: Land? = null
)

enum class LandType {
    Depths,
    Forest,
    Aerie,
    Caves,
    Darklands,
    Dreamlands
}
