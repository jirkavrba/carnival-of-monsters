package dev.vrba.carnival.game

data class GameConfiguration(
    val lands: Map<Land, Int>,
    val monsters: Map<Monster, Int>,
    val characters: Map<Character, Int>,
    val letters: Map<SecretLetterTask, Int>,
    val newspapers: Map<NewspaperAction, Int>
) {
    fun createCardDeck(): List<GameCard> {
        val cards =
            mapCards(lands) { LandCard(it) } +
            mapCards(monsters) { MonsterCard(it) } +
            mapCards(characters) { CharacterCard(it) } +
            mapCards(letters) { SecretLetterCard(it) } +
            mapCards(newspapers) { NewspaperCard(it) }

        return cards.shuffled()
    }

    private inline fun <T, S : GameCard> mapCards(cards: Map<T, Int>, transform: (T) -> S): List<S> {
        return cards.flatMap { (item, count) -> List(count) { transform(item) } }
    }
}

private inline fun <reified T> forEveryLandType(transform: (LandType) -> T): Array<T> {
    return LandType.values()
        .filter { it != LandType.Dreamlands }
        .map { transform(it) }
        .toTypedArray()
}

val defaultGameConfiguration = GameConfiguration(
    mapOf(
        * forEveryLandType { Land(it, 1, 0) to 10 },
        * forEveryLandType { Land(it, 2, 1) to 1 },
        * forEveryLandType { Land(it, 3, 2) to 1 },

        Land(LandType.Dreamlands, 3, 0) to 7
    ),
    mapOf(
        * forEveryLandType { Monster(it, 1, 0, 0, 3) to 4 },
        * forEveryLandType { Monster(it, 2, 0, 0, 6) to 2 },
        * forEveryLandType { Monster(it, 3, 1, 0, 11) to 2 },
        * forEveryLandType { Monster(it, 2, 1, 0, 8) to 1 },
        * forEveryLandType { Monster(it, 3, 2, 0, 13) to 1 },
        * forEveryLandType { Monster(it, 4, 2, 0, 16) to 1 },
        * forEveryLandType { Monster(it, 1, 0, 1, 1) to 1 },
        * forEveryLandType { Monster(it, 2, 0, 1, 4) to 1 },

        Monster(LandType.Dreamlands, 3, 0, 0, 4) to 1,
        Monster(LandType.Dreamlands, 5, 0, 0, 6) to 1,
        Monster(LandType.Dreamlands, 7, 1, 0, 10) to 1,
        Monster(LandType.Dreamlands, 8, 2, 0, 13) to 1,
        Monster(LandType.Dreamlands, 9, 2, 2, 10) to 1,
        Monster(LandType.Dreamlands, 10, 3, 2, 13) to 1,
        Monster(LandType.Dreamlands, 4, 0, 1, 3) to 1,
        Monster(LandType.Dreamlands, 6, 0, 1, 5) to 1,
    ),
    mapOf(
        * forEveryLandType { Character(3, LandTypeRewards(it)) to 1 },
        Character(2, LandPlaceholder) to 4,
        Character(2, IgnoreDangerSymbol) to 2,
        Character(3, NewspaperRewards) to 1,
        Character(3, DangerSymbolsReward) to 1,
    ),
    mapOf(
        * forEveryLandType { MonstersWithLandTypeTask(it) to 1 },
        FreeFourPointsTask to 1,
        DangerSymbolsTask to 1,
        DreamlandsTask to 1,
        Level1MonstersTask to 1,
        DistantLandsTask to 1,
        LoansRewardTask to 1,
        CagesRewardTask to 1,
        LettersRewardTask to 1,
        CharactersRewardTask to 1,
        NoLoansRewardTask to 1,
        AllLandsExploredTask to 1,
        FewLandsExploredTask to 1,
        StrongMonstersTask to 1,
        DeeplyExploredLandTask to 1,
    ),
    mapOf(
        * forEveryLandType { LandsRewardAction(it) to 1 },
        CageAction to 7,
        GoldAction to 7,
        CharactersRewardAction to 1,
    )
)