package dev.vrba.carnival.game

data class Player(
    val id: Int,

    val username: String,

    // The currently selected card
    val capturedCard: GameCard? = null,

    // Number of cards available for drafting
    val draftingCards: List<GameCard> = emptyList(),

    // Cards kept instead of playing them
    val keptCards: List<GameCard> = emptyList(),

    // Cards that are no longer in play
    val menagerie: List<GameCard> = emptyList(),

    // Number of coins that the player owns
    val gold: Int = 0,

    // Number of cages that the player owns
    val cages: Int = 0,

    // Lands played by the player
    val lands: List<Land> = emptyList(),

    // Characters that the player has hired
    val characters: List<Character> = emptyList(),

    // The number of loans that the player has taken
    val loans: Int = 0,

    // Whether the player has finished drafting and casting creatures
    val finished: Boolean = false,
)