package dev.vrba.carnival.game

data class Game(
    val players: List<Player>,
    val drawingDeck: List<GameCard>,
    val currentSeason: LandType,
    val phase: GamePhase
)

enum class GamePhase {
    Drafting, // Players are drafting cards from the deck
    Casting // Players have time to cast all their creatures stored in the kept cards deck
}