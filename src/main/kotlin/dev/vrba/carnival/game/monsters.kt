package dev.vrba.carnival.game

data class Monster(
    val type: LandType,
    val level: Int,
    val danger: Int,
    val cards: Int,
    val points: Int
)